def ground_roll(log_file):

    # POSITIONAL LIMITS
    GPS_LOWER =  0 # RESET BASED ON AIRSTRIP
    GPS_UPPER_ROLL = GPS_LOWER + 3
    GPS_UPPER_OBSTACLE = GPS_LOWER + 15.24

    # ANGULAR LIMITS
    ECU_ROLL_LOWER = -3.5
    ECU_ROLL_UPPER = 3.5

    # VELOCITY LIMITS
    AIRSPEED_TAKEOFF = 43.727
    GPS_DIFF_LOWER = -3
    GPS_DIFF_UPPER = 3

    #ANGULAR RATE LIMITS
    ROLL_RATE_UPPER = 2
    ROLL_RATE_LOWER = -2    
    YAW_RATE_UPPER = 2
    YAW_RATE_LOWER = -2
    PITCH_RATE_UPPER = 2
    PITCH_RATE_LOWER = -2
  
    # Maxiumum Takeoff Weight For Flight Test of 20210720-Test_03.csv

    MTOW = 2024 # kg

    path = os.path.dirname(os.path.realpath(__file__))+"/put_csvs_here/"+log_file

    print(path,"is open.")

    def PUT_FLIGHT_MODE_HERE_points(path) -> pd.DataFrame:
        """Find and return a dataframe of _____ points for this flight.
        Args:
            path (str): path to flight test dataframe
        Returns:
            pd.DataFrame: dataframe containing ______ points for this flight
        """

        ## Do not change the index at this point! It is necessary for the following steps
        index = ['Timestamp']

        ## Here we define arrays (or lists) of the COLUMN names that we wish to import to our dataframe.
        ## I like to group them by type of variable, I.E. velocities, motor_info, etc.

        velocities = ['True Airspeed', 'Airspeed', 'Ground Speed', 'Climb Rate', 'ECU Pitch Rate','ECU Yaw Rate','ECU Roll Rate', 'Angle of Attack']
        positions = ['GPS Altitude']
        angles = ['ECU Roll Angle', 'ECU Pitch Angle', 'ECU Yaw Angle']

        ## Let usecols be the sum of the lists defined above INCLUDING the index
        usecols = index + velocities + positions + angles

        ## If desired use the list differentiables. Every column here will be automatically differentiated and added to the dataframe with a naming convention similar to "True Airspeed Diff"
        differentiables = velocities + positions + angles

        ## Import the CSV to the Pandas DataFrame

        df = pd.read_csv(path, usecols=usecols)

        ## Don't mess with this code too much unless you need to...
        ## This is where we parse the Timestamps in the CSV into a usable "Seconds Elapsed"  column, and re attach it to the data frame
        ## This is also where the data gets resampled to the desired finite time step interval.

        df['Datetime'] = pd.to_datetime(df['Timestamp'], format='%H:%M:%S:%f')
        df.set_index('Datetime', inplace=True)

        ## RESAMPLE
        df = df.resample('1s').mean()
        df['Elapsed'] = (df.index - pd.Timestamp("1970-01-01")) // pd.Timedelta('1ns')
        df['Elapsed'] = (df['Elapsed'] - df['Elapsed'].iloc[0]) / 10**9

        ## A quick for loop to differentiate all the differentiables columns
        for column in differentiables:
            df[column + " Diff"] = (df[column].diff(periods=1) / df['Elapsed'].diff(periods=1))

        GPS_LOWER = df['GPS Altitude'].min()


        ## Here is the actual data filter. Define the the filter name is on the left, the middle is the column to filter by, nad the right is the less than or more than condition
        ## (filter_name) = df['Column Name'] > (Constant)

        # POSITIONAL Filters

        gps_lower = df['GPS Altitude'] > GPS_LOWER ## RESET BASED ON AIRSTRIP
        gps_upper_roll = df['GPS Altitude'] < GPS_LOWER + 3
        gps_upper_obstacle = df['GPS Altitude'] < GPS_LOWER + 15.24

        # ANGULAR Filters

        ecu_roll_lower = df['ECU Roll Angle'] > ECU_ROLL_LOWER
        ecu_roll_upper = df['ECU Roll Angle'] < ECU_ROLL_UPPER

        # VELOCITY Filters

        airspeed_condition = df['True Airspeed'] < AIRSPEED_TAKEOFF + 10
        gps_diff_lower = df['GPS Altitude_differential'] > GPS_DIFF_LOWER
        gps_diff_upper = df['GPS Altitude_differential'] < GPS_DIFF_UPPER


        ## Here we define a new data frame that is filtered by the list of filters defined above!

        PUT_FLIGHTMODE_HERE_points = df[(gps_diff_lower) & (gps_diff_upper) & (airspeed_condition) & (ecu_roll_lower) & (ecu_roll_upper) & 
                        (yaw_rate_upper) & (yaw_rate_lower) & (gps_upper_obstacle) & (gps_lower)
                        (pitch_rate_upper) & (pitch_rate_lower) & (roll_rate_upper) & (roll_rate_lower) ]

        return roll_points